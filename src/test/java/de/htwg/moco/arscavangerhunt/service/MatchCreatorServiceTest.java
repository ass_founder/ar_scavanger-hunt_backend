package de.htwg.moco.arscavangerhunt.service;

import de.htwg.moco.arscavangerhunt.Application;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import de.htwg.moco.arscavangerhunt.serivce.HuntSearchService;
import de.htwg.moco.arscavangerhunt.serivce.MatchCreatorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class MatchCreatorServiceTest {

    @Autowired
    private MockMvc mvc;

    private String huntName = "hunt";

    @Mock
    private HuntSearchService searchService;

    @InjectMocks
    private MatchCreatorService service;

    @Before
    public void init(){
        mvc = MockMvcBuilders.standaloneSetup(service).build();
        Hunt hunt = mock(Hunt.class);
        when(searchService.searchHuntForName(huntName, huntName)).thenReturn(hunt);
    }
    @Test
    public void testCreation(){
        Match session = this.service.createSession(huntName, huntName, huntName);
        assertTrue(session != null);
    }

    @Test
    public void testDefault(){
        assertTrue(this.service.createDefaultSession(huntName) != null);
    }
}
