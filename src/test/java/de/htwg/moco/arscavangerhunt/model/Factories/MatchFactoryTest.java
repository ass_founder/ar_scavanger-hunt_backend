package de.htwg.moco.arscavangerhunt.model.Factories;


import com.google.firebase.database.DataSnapshot;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import org.junit.*;
import org.mockito.*;
import static org.junit.Assert.*;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MatchFactoryTest {

    @Test
    public void testFactoryWithNull(){
        Match matchFromDataSnapshot = MatchFactory.createMatchFromDataSnapshot(null);
        assertTrue(matchFromDataSnapshot == null);
    }

    @Test
    public void testFactoryWithWrongSnapshot(){
        try{
            DataSnapshot snapshot = mock(DataSnapshot.class);
            when(snapshot.child(anyString())).thenReturn(snapshot);
            when(snapshot.getValue()).thenReturn(false);
            MatchFactory.createMatchFromDataSnapshot(snapshot);
            fail("Exception Needed");
        }catch (Exception e){
            assertTrue(e != null);
        }
    }
}
