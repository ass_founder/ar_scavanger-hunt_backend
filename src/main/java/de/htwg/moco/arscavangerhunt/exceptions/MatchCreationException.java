package de.htwg.moco.arscavangerhunt.exceptions;

public class MatchCreationException extends Exception {
    public MatchCreationException() { super(); }
    public MatchCreationException(String message) { super(message); }
    public MatchCreationException(String message, Throwable cause) { super(message, cause); }
    public MatchCreationException(Throwable cause) { super(cause); }
}
