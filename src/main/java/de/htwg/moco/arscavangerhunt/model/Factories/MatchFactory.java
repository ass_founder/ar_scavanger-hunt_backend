package de.htwg.moco.arscavangerhunt.model.Factories;

import com.google.firebase.database.DataSnapshot;
import de.htwg.moco.arscavangerhunt.model.DefaultHuntImpl;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.multiplayer.DefaulMatchImpl;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class MatchFactory {

    public static String KEY_MATCH_ID = "matchID";

    public static String KEY_BITS_MOST = "mostSignificantBits";

    public static String KEY_BITS_LEAST = "leastSignificantBits";

    public static String KEY_NAME = "name";

    public static String KEY_STARTED = "started";

    public static String KEY_PLAYER_POINTS = "playerPoints";

    public static String KEY_WINING_POINTS= "winingPoints";

    public static String KEY_HUNT = "hunt";

    public static Match createMatchFromDataSnapshot(DataSnapshot snapshot){
        if(snapshot != null){
            DefaulMatchImpl defaulMatch = new DefaulMatchImpl();
            //convert the part where firebase messes the uuid up to something usefull
            HashMap<String, Long> matchID = (HashMap<String, Long>) snapshot.child(KEY_MATCH_ID).getValue();
            Long mostBits = matchID.get(KEY_BITS_MOST);
            Long leastBits = matchID.get(KEY_BITS_LEAST);
            defaulMatch.setMatchID(new UUID(mostBits, leastBits));
            String name = (String) snapshot.child(KEY_NAME).getValue();
            defaulMatch.setName(name);
            //look if the match started
            boolean started = (boolean) snapshot.child(KEY_STARTED).getValue();
            if(started){
                defaulMatch.startGame();
            }
            Map<String, Long> playerPoints = (Map<String, Long>) snapshot.child(KEY_PLAYER_POINTS).getValue();
            defaulMatch.setPlayerPoints(playerPoints);

            Long winingPoints = (Long) snapshot.child(KEY_WINING_POINTS).getValue();

            defaulMatch.setWiningPoints(winingPoints.intValue());
            Hunt hunt = snapshot.child(KEY_HUNT).getValue(DefaultHuntImpl.class);
            defaulMatch.setHunt(hunt);
            return defaulMatch;
        }
            return null;
    }
}
