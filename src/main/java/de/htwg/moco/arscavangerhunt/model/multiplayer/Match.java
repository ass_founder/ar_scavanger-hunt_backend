package de.htwg.moco.arscavangerhunt.model.multiplayer;

import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.Location;

import java.util.Set;
import java.util.UUID;

public interface Match {

    UUID getMatchID();

    void setMatchID(UUID matchID);

    void addPlayer(String player);

    void removePlayer(String player);

    boolean addPoints(String player, int points);

    String endGame();

    boolean hasLocation(Location location);

    void startGame();

    Set<String> getPlayers();

    Hunt getHunt();

    long getPoints(String player);

    boolean isStarted();

}
