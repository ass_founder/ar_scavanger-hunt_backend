package de.htwg.moco.arscavangerhunt.model;

import java.util.ArrayList;
import java.util.List;

public class DefaultHuntImpl implements Hunt {

    private List<Location> locations = new ArrayList<>();

    private String name;

    private String author;

    @Override
    public List<Location> getLocations() {
        return locations;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @Override
    public void addLocation(Location location) {
        locations.add(location);
    }

    @Override
    public void removeLocation(Location location) {
        locations.remove(location);
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (Location location : locations) {
                builder.append(location.toString());
        }
        return String.format("Name: from %s contains %s", name, author, builder.toString());
    }

}
