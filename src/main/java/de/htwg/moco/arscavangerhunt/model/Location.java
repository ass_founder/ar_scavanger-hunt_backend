package de.htwg.moco.arscavangerhunt.model;


public class Location{

    private double longitude;

    private double latitude;

    private double altitude;

    private String name;

    public Location(){/*Default for Default reasons*/}

    public Location(double x, double y, double z, String name){
        this.longitude = x;
        this.latitude = y;
        this.altitude = z;
        this.name = name;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return "name: "+ this.name +"\n\tlongitude: " + longitude +"\n\tlatitude: " + latitude + "\n\t altitude:"+ altitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;
        return this.name.equals(location.getName());
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(longitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(altitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result + name.hashCode();
    }
}
