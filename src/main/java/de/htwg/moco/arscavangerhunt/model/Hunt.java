package de.htwg.moco.arscavangerhunt.model;

import java.util.List;

public interface Hunt {

    List<Location> getLocations();

    String getName();

    void setName(String name);

    void setAuthor(String author);

    String getAuthor();

    void setLocations(List<Location> locations);

    void addLocation(Location location);

    void removeLocation(Location location);

}
