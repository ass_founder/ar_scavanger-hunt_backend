package de.htwg.moco.arscavangerhunt.model.multiplayer;

import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.Location;

import java.util.*;

public class DefaulMatchImpl implements Match {

    private UUID matchID = UUID.randomUUID();

    private String name;

    private Hunt hunt;

    //Can´t use Integer because Integer has no default construcotr but firebase needs that
    private Map<String, Long> playerPoints;

    private int winingPoints;

    private boolean started = false;

    public DefaulMatchImpl(){/*Default Constructir for default reasons*/}

    public DefaulMatchImpl(String name, Hunt hunt, int winingPoints) {
        this.name = name;
        this.hunt = hunt;
        this.winingPoints = winingPoints;
        playerPoints = new HashMap<>();

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Hunt getHunt() {
        return hunt;
    }

    public void setHunt(Hunt hunt) {
        this.hunt = hunt;
    }

    public void setMatchID(UUID matchID){
        this.matchID = matchID;
    }

    public Map<String, Long> getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerPoints(Map<String, Long> playerPoints) {
        this.playerPoints = playerPoints;
    }

    public int getWiningPoints() {
        return winingPoints;
    }

    public void setWiningPoints(int winingPoints) {
        this.winingPoints = winingPoints;
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    @Override
    public UUID getMatchID() {
        return matchID;
    }

    @Override
    public void addPlayer(String player) {

        playerPoints.put(player, 0l);
    }

    @Override
    public void removePlayer(String player) {
        playerPoints.remove(player);
    }

    @Override
    public boolean addPoints(String player, int points) {

       if(points > 0){
           Long currentValue = playerPoints.get(player);
           long i =  currentValue.longValue();
           long newPoints = i+points;
           playerPoints.put(player, newPoints);
           if(newPoints >= winingPoints){
                return true;
           }

       }
       return false;
    }


    @Override
    public String endGame() {
        long maxPoints = 0;
        String winner = null;
        for (String player : playerPoints.keySet()) {
           long points = playerPoints.get(player).longValue();
            if(points > maxPoints){
                maxPoints = points;
                winner = player;
            }
        }
        return winner;
    }

    @Override
    public boolean hasLocation(Location location) {
        return hunt.getLocations().contains(location);
    }

    @Override
    public void startGame() {
        this.started = true;
    }

    @Override
    public Set<String> getPlayers() {
        return playerPoints.keySet();
    }

    @Override
    public long getPoints(String player) {
        return playerPoints.get(player);
    }

    @Override
    public String toString(){
        return String.format("Name: %s \n\t Match: %s \n\t Hunt: %s \n\t PointsToWin: %s\n\t started; %s\n\t players: %s",
                name, matchID, hunt, winingPoints, started, playerPoints);
    }
}
