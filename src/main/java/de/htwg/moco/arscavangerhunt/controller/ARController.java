package de.htwg.moco.arscavangerhunt.controller;


import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/")
public class ARController {

    @GetMapping
    public ResponseEntity<?> loadARElements(){
        String response = "<html><head><title>hallo</title></head><body>yooo</body></html>";
        LoggerService.logInfo(this.getClass().getName(), "AR-Elements loaded");
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
}
