package de.htwg.moco.arscavangerhunt.controller;

import de.htwg.moco.arscavangerhunt.model.Location;
import de.htwg.moco.arscavangerhunt.model.multiplayer.DefaulMatchImpl;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import de.htwg.moco.arscavangerhunt.repositories.PersistanceService;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import de.htwg.moco.arscavangerhunt.serivce.MatchCreatorService;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/sessions", produces = MediaType.APPLICATION_JSON_VALUE)
public class SessionController {

    @Resource
    private MatchCreatorService matchCreatorService;

    @Resource
    PersistanceService persistanceService;

    @GetMapping("/getAvailableSessions")
    public ResponseEntity<?>  getAvailableMatches() {
        List<Match> availableMatches = persistanceService.getAvailableMatches();
          return new ResponseEntity<Object>(availableMatches, HttpStatus.OK);
    }

    /**
     * This method creates a new Session with the default hunt.
     * @param userName
     * @return
     */
    @GetMapping("/createNew")
    public ResponseEntity<?> createNewSession(@RequestParam String userName) {

        DefaulMatchImpl defaultSession = (DefaulMatchImpl) matchCreatorService.createDefaultSession(userName);
        defaultSession.addPlayer(userName);
        persistanceService.saveMatch(defaultSession);
        String responseValue = "ID: " + defaultSession.getMatchID();
        return new ResponseEntity<Object>(responseValue,HttpStatus.OK);
    }

    @GetMapping("/createSession")
    public ResponseEntity<?> createSession(@RequestParam String name, @RequestParam String huntName, @RequestParam String userName){

        String decodedName = name;
        String decodedHuntName = huntName;

        try {
            decodedName = URLDecoder.decode(name, "UTF-8");
            decodedHuntName = URLDecoder.decode(huntName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LoggerService.logError(this.getClass().getSimpleName(),e);
        }


        Match session = matchCreatorService.createSession(decodedName, decodedHuntName, userName);
        if(session == null){
            LoggerService.logInfo(this.getClass().getName(), "Couldn´t create session for " +userName);
            return new ResponseEntity<Object>("Hunt not available", HttpStatus.BAD_REQUEST);
        }
        session.addPlayer(userName);
        persistanceService.saveMatch(session);
        LoggerService.logInfo(this.getClass().getName(), "created Session " + session.getMatchID());
        return new ResponseEntity<Object>(session.getMatchID(), HttpStatus.OK);

    }

    @GetMapping("/deleteSession")
    public ResponseEntity<?> deleteSession(@RequestParam String sessionId){

        persistanceService.deleteMatch(sessionId);
        LoggerService.logInfo(this.getClass().getName(), "delete Session " + sessionId);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @GetMapping("/joinSession")
    public ResponseEntity<Object> joinMatch(@RequestParam String matchID, @RequestParam String playerName) {
        persistanceService.jointMatch(matchID, playerName);
        return new ResponseEntity<Object>(HttpStatus.OK);

    }

    @GetMapping("/leaveSession")
    public ResponseEntity<Object> leaveMatch(@RequestParam String matchID, @RequestParam String playerName) {
        persistanceService.leaveMatch(matchID, playerName);
        return new ResponseEntity<Object>(HttpStatus.OK);

    }

    @GetMapping("/startGame")
    public ResponseEntity<Object> startGame(@RequestParam String matchID) {
        persistanceService.startGame(matchID);
        return new ResponseEntity<Object>(HttpStatus.OK);

    }

    @GetMapping("/addPoints")
    public ResponseEntity<Object> addPoints(@RequestParam String matchID, @RequestParam String playerName, @RequestParam double x, @RequestParam double y, @RequestParam double z, @RequestParam String locationName){
        Location location = new Location(x, y, z, locationName);
        persistanceService.addPoints(matchID, playerName, location);
        String message = "You got a point";
        return new ResponseEntity<Object>(message, HttpStatus.OK);
    }

    @MessageMapping("/listen")
    @SendTo ("/session/updates")
    public String listener(String message){
        System.out.println(message);
        return message;
    }
}
