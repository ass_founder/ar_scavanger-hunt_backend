package de.htwg.moco.arscavangerhunt.controller;


import de.htwg.moco.arscavangerhunt.model.Location;
import de.htwg.moco.arscavangerhunt.repositories.FireBasePerstanceServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/locations", produces = MediaType.APPLICATION_JSON_VALUE)
public class LocationController {

    @Resource
    private FireBasePerstanceServiceImpl fireBasePerstanceService;

    @GetMapping(path = "/saveLocation")
    public ResponseEntity<?> saveLocation(@RequestParam double x, @RequestParam double y, @RequestParam double z, @RequestParam String locationName) {
        try {
            Location testLocation = new Location(x, y, z, locationName);
            fireBasePerstanceService.saveLocation(testLocation);
        } catch (Exception e) {
            //just in case
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Object>(null, HttpStatus.OK);
    }

}


