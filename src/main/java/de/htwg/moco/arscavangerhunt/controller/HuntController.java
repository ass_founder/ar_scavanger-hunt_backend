package de.htwg.moco.arscavangerhunt.controller;

import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.repositories.PersistanceService;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = "/hunts", produces = MediaType.APPLICATION_JSON_VALUE)
public class HuntController {

    @Resource
    private PersistanceService persistanceService;

    @GetMapping("/huntsPerUser")
    private ResponseEntity<?> getHuntsPerUser(@RequestParam String userName){
        List<Hunt> huntsPerUser = persistanceService.getHuntsPerUser(userName);
        if(huntsPerUser.isEmpty()){
            LoggerService.logWarning(this.getClass().getName(), "Empty hunts loaded for " +userName);
            return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity<Object>(huntsPerUser, HttpStatus.OK);
        }
    }

}
