package de.htwg.moco.arscavangerhunt.serivce;

import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.repositories.PersistanceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@Service
public class HuntSearchService {

    @Resource
    private PersistanceService persistanceService;

    public Hunt searchHuntForName(String name,@NotNull String userName){

        return persistanceService.getHuntPerName(name, userName);
    }
}
