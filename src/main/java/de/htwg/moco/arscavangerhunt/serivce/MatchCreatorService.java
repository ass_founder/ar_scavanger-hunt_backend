package de.htwg.moco.arscavangerhunt.serivce;

import de.htwg.moco.arscavangerhunt.model.DefaultHuntImpl;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.Location;
import de.htwg.moco.arscavangerhunt.model.multiplayer.DefaulMatchImpl;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class MatchCreatorService {

    @Resource
    private HuntSearchService huntSearchService;

    public Match createDefaultSession(String creator) {
        DefaultHuntImpl hunt = new DefaultHuntImpl();
        hunt.setName("testHunt");
        hunt.setAuthor(creator);
        hunt.addLocation(new Location(10, 10, 15, "Testlocation"));
        return new DefaulMatchImpl("SessionName", hunt, 1);
    }

    public Match createSession(String name, String huntName, String userName) {
        Hunt hunt = huntSearchService.searchHuntForName(huntName, userName);
        if (hunt == null) {
            return null;
        }
        return new DefaulMatchImpl(name, hunt, hunt.getLocations().size());
    }
}
