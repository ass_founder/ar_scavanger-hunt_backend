package de.htwg.moco.arscavangerhunt.serivce;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class LoggerService {

    private static final String LOGGER_NAME = "AR_logger";

    private static final String LOGGER_FORMA = "%s: %s";

    private static final Logger logger = Logger.getLogger(LOGGER_NAME);

    public static void initLogger(){
      try {
          String directory=System.getProperty("user.dir");
          Path path = Paths.get(directory, "server.out");
          FileHandler fh = new FileHandler(path.toAbsolutePath().toString());
          SimpleFormatter formatter = new SimpleFormatter();
          fh.setFormatter(formatter);
          logger.addHandler(fh);
          logInfo("TEST", "§");
      }catch (Exception e){
          logError(LoggerService.class.getName(), e);
      }
    }

    public static void logInfo(String classname, String info){
        logger.info(createMessage(classname, info));
    }

    public static void logWarning(String classname, String info){
        logger.warning(createMessage(classname, info));
    }

    public static void logError(String className, Throwable e){
        logger.severe(String.format("%s: %s", className, e.getMessage()));
    }

    private static String createMessage(String className, String content){
        return String.format(LOGGER_FORMA, className, content);
    }
}
