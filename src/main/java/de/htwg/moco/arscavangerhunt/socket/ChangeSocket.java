package de.htwg.moco.arscavangerhunt.socket;


import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import de.htwg.moco.arscavangerhunt.serivce.LoggerService;

public class ChangeSocket {

    private List<ConnectionToClient> clientList;
    private LinkedBlockingQueue<Object> messages;
    private ServerSocket serverSocket;

    public ChangeSocket(int port) {
        try {
            clientList = new ArrayList<ConnectionToClient>();
            messages = new LinkedBlockingQueue<Object>();
            serverSocket = new ServerSocket(port);
            LoggerService.logInfo("Address", serverSocket.getInetAddress().getHostAddress());
            Thread accept = new Thread(() -> {
                while (true) {
                    try {
                        Socket s = serverSocket.accept();
                        clientList.add(new ConnectionToClient(s));
                        LoggerService.logInfo(this.getClass().getName(), "Socketconection Open");
                    } catch (IOException e) {
                        LoggerService.logError(this.getClass().getName(), e);
                    }
                }
            });
            accept.setDaemon(true);
            accept.start();
        } catch (Exception e) {
            LoggerService.logError(this.getClass().getName(), e);
        }
    }

    public void sendToAll(String message) {
        LoggerService.logInfo(this.getClass().getName(), message + " send to " + clientList.size() + " clients");
        Iterator<ConnectionToClient> iterator = clientList.iterator();
        while (iterator.hasNext()) {
            ConnectionToClient next = iterator.next();
            if(next.stillOpen()){
                next.write(message);
            }else{
                LoggerService.logInfo(this.getClass().getName(), "stop sending to a connection because it seems to have left");
                iterator.remove();
            }
        }
    }


    private class ConnectionToClient {
        DataInputStream in;
        PrintStream out;
        Socket socket;

        ConnectionToClient(Socket socket) throws IOException {
            this.socket = socket;
            in = new DataInputStream(socket.getInputStream());
            out = new PrintStream(socket.getOutputStream());
        }

        public void close() {
            try {
                this.socket.getOutputStream().close();
                this.socket.getInputStream().close();
                this.socket.close();
                LoggerService.logInfo(this.getClass().getName(), "Close one connection");
            } catch (Exception e) {
                LoggerService.logError(this.getClass().getName(), e);
            }
        }

        public boolean stillOpen(){
            try{
                if(in.read() == -1){
                    return false;
                }
                if(socket.isClosed()){
                    return false;
                }
            }catch (Exception e){
                return false;
            }
            return true;
        }


        public void write(String obj) {
            try {
                out.write(obj.getBytes());
                out.write(" end\n".getBytes());
                out.flush();
            } catch (Exception e) {
                LoggerService.logError(this.getClass().getName(), e);
                close();
            }
        }
    }
}


