package de.htwg.moco.arscavangerhunt.socket;

import de.htwg.moco.arscavangerhunt.constants.SocketConstants;


public final class SocketService {

    private static ChangeSocket socket = new ChangeSocket(SocketConstants.SOCKET_PORT);

    public static void sendMessage(String message){
        socket.sendToAll(message);
    }
}
