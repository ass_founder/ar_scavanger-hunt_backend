package de.htwg.moco.arscavangerhunt.repositories.listener;

import com.google.firebase.database.*;
import de.htwg.moco.arscavangerhunt.constants.DatabaseKeyConstants;
import de.htwg.moco.arscavangerhunt.model.DefaultHuntImpl;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;

import java.util.*;

public class HuntsCreatedPerUserListenerImpl implements DatabaseListener<Hunt> {

    private Map<String, List<Hunt>> values = new HashMap<>();

    private Set<String> userIds = new HashSet<>();

    @Override
    public String getListenerName() {
        return DatabaseKeyConstants.KEY_HUNTS_CREATED_USER;
    }

    @Override
    public void startQuery(FirebaseDatabase database) {

        database.getReference(DatabaseKeyConstants.KEY_USER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String name = snapshot.getKey();
                        userIds.add(name);
                        registerListenerforUserName(name, database);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public Collection<Hunt> getValues() {
        List<Hunt> allHunts = new ArrayList<>();
        for (String key: values.keySet()){
            allHunts.addAll(values.get(key));
        }
        return allHunts;
    }

    private void registerListenerforUserName(String name, FirebaseDatabase database){
        database.getReference(DatabaseKeyConstants.KEY_HUNT).orderByChild("author").equalTo(name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot listenerSnapshot) {
                List<Hunt> huntsForUser = new ArrayList<>();
                for (DataSnapshot huntSnapShot : listenerSnapshot.getChildren()) {
                    Hunt hunt = huntSnapShot.getValue(DefaultHuntImpl.class);
                    huntsForUser.add(hunt);
                }
                values.put(name, huntsForUser);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public Map<?, List<Hunt>> getValuesAsMap() {
        return values;
    }
}
