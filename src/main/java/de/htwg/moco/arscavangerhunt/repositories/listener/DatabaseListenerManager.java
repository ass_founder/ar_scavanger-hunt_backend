package de.htwg.moco.arscavangerhunt.repositories.listener;

import com.google.firebase.database.FirebaseDatabase;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DatabaseListenerManager {

    private Map<String, DatabaseListener> registeredListener;

    private static final String DATABASE_LISTENER_LOCATION = DatabaseListenerManager.class.getPackage().getName();

    public DatabaseListenerManager(FirebaseDatabase firebaseDatabase){

        Reflections reflections = new Reflections(DATABASE_LISTENER_LOCATION);
        //gets every impl of the interface
        registeredListener = new HashMap<>();
        for (Class<? extends DatabaseListener> listenerClass : reflections.getSubTypesOf(DatabaseListener.class)) {
            Constructor<?> constructor = listenerClass.getConstructors()[0];
            try {
                //cast generic constructor to instance of Databaseclass
                DatabaseListener listener = (DatabaseListener) constructor.newInstance();
                listener.startQuery(firebaseDatabase);
                //put it into the map
                registeredListener.put(listener.getListenerName(), listener);
            } catch (Exception e){
            LoggerService.logError(this.getClass().getName(), e);
            }

        }

    }

    public Collection<?> getValuesFormListener(String listenerName){
        DatabaseListener listener = registeredListener.get(listenerName);
        if(listener != null){
            return listener.getValues();
        }
        return Collections.emptyList();
    }

    public Map<?, ?> getValuesFromListenerAsMap(String listenerName){
        DatabaseListener listener = registeredListener.get(listenerName);
        if( listener != null){
            return listener.getValuesAsMap();
        }else{
            return Collections.emptyMap();
        }
    }

}
