package de.htwg.moco.arscavangerhunt.repositories;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import de.htwg.moco.arscavangerhunt.constants.DatabaseKeyConstants;
import de.htwg.moco.arscavangerhunt.model.Factories.MatchFactory;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.Location;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import de.htwg.moco.arscavangerhunt.repositories.listener.DatabaseListenerManager;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.*;

@Service
public class FireBasePerstanceServiceImpl implements PersistanceService {

    private FirebaseDatabase firebaseDatabase;

    private DatabaseListenerManager databaseListenerManager;

    @Autowired
    public FireBasePerstanceServiceImpl(FirebaseDatabase firebaseDatabase) {

        this.firebaseDatabase = firebaseDatabase;
        this.databaseListenerManager = new DatabaseListenerManager(firebaseDatabase);
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if(!snapshot.hasChild("playerPoints")){
                        firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(snapshot.getKey()).removeValueAsync();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void saveHunt(Hunt hunt) {
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_HUNT).push().setValueAsync(hunt);
    }

    @Override
    public void saveMatch(Match match) {
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(match.getMatchID().toString()).setValueAsync(match);
    }

    @Override
    public void deleteMatch(String sessionId) {
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(sessionId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Match matchFromDataSnapshot = MatchFactory.createMatchFromDataSnapshot(dataSnapshot);
                Set<String> players = matchFromDataSnapshot.getPlayers();
                if(players.size() <=1){
                    deleteSession();
                }else{
                    for (String player: players){
                        if(matchFromDataSnapshot.getPoints(player) >= matchFromDataSnapshot.getHunt().getLocations().size()){
                            deleteSession();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

            private  void deleteSession(){
                firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(sessionId).removeValueAsync();
            }
        });
    }

    @Override
    public void saveLocation(Location location) {
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_LOCATION).push().setValueAsync(location);
    }

    @Override
    public List<Match> getAvailableMatches() {
        return (List<Match>) databaseListenerManager.getValuesFormListener(DatabaseKeyConstants.KEY_MATCH_NOT_STARTED);
    }

    @Override
    public void jointMatch(String matchID, String player) {
        List<Match> matches = new ArrayList<>();
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).addListenerForSingleValueEvent(new ValueEventListener() {

            private boolean changed = false;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!changed) {
                    Match matchFromDataSnapshot = MatchFactory.createMatchFromDataSnapshot(dataSnapshot);
                    matchFromDataSnapshot.addPlayer(player);
                    firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).setValueAsync(matchFromDataSnapshot);
                    matches.add(matchFromDataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public void leaveMatch(String matchID, String player) {
        List<Match> matches = new ArrayList<>();
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).addListenerForSingleValueEvent(new ValueEventListener() {

            private boolean changed = false;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!changed) {
                    Match matchFromDataSnapshot = MatchFactory.createMatchFromDataSnapshot(dataSnapshot);
                    matchFromDataSnapshot.removePlayer(player);
                    firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).setValueAsync(matchFromDataSnapshot);
                    matches.add(matchFromDataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public void startGame(String matchID) {
        List<Match> matches = new ArrayList<>();
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).addListenerForSingleValueEvent(new ValueEventListener() {

            private boolean changed = false;

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!changed) {
                    Match matchFromDataSnapshot = MatchFactory.createMatchFromDataSnapshot(dataSnapshot);
                    matchFromDataSnapshot.startGame();
                    firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).setValueAsync(matchFromDataSnapshot);
                    matches.add(matchFromDataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    public void addPoints(String matchID, String player, Location location) {
        this.firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).addListenerForSingleValueEvent(new ValueEventListener() {

            private boolean changed = false;

            private String message = "";

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!changed) {
                    Match match = MatchFactory.createMatchFromDataSnapshot(dataSnapshot);
                    if (match.hasLocation(location)) {
                        boolean winning = match.addPoints(player, 1);
                        //TODO send message over firebase
                        if (winning) {
                            String winner = match.endGame();
                            message = String.format("The winner is: %s", winner);
                        } else {
                            message = String.format("Added points, the game is not over");
                        }
                        changed = true;
                        firebaseDatabase.getReference(DatabaseKeyConstants.KEY_MATCH).child(matchID).setValueAsync(match);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public List<Hunt> getHuntsPerUser(String userName) {
        Object o = databaseListenerManager.getValuesFromListenerAsMap(DatabaseKeyConstants.KEY_HUNTS_CREATED_USER).get(userName);
        if (o == null){
            return Collections.emptyList();
        }
        return (List<Hunt>) o;
    }

    @Override
    public Hunt getHuntPerName(String name, @NotNull String userName) {
        Map<String, List<Hunt>> valuesFromListenerAsMap = (Map<String, List<Hunt>>) databaseListenerManager.getValuesFromListenerAsMap(DatabaseKeyConstants.KEY_HUNTS_PER_NAME);
        if(valuesFromListenerAsMap.get(name) == null){
            return null;
        }
        Hunt foundHunt = null;
        for (Hunt hunt: valuesFromListenerAsMap.get(name)){
            if (userName.equals(hunt.getAuthor())){
                foundHunt = hunt;
            }
        }
        return foundHunt;
    }

}
