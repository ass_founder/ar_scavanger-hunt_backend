package de.htwg.moco.arscavangerhunt.repositories.listener;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface DatabaseListener<T> {

    String getListenerName();

    void startQuery(FirebaseDatabase database);

    Collection<T> getValues();

    Map<?, List<T>> getValuesAsMap();

}
