package de.htwg.moco.arscavangerhunt.repositories;

import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.model.Location;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;

import java.util.List;
import java.util.Map;

public interface PersistanceService {

    void saveHunt(Hunt hunt);

    void saveMatch(Match match);

    void deleteMatch(String sessionId);

    void saveLocation(Location location);

    List<Match> getAvailableMatches();

    void jointMatch(String matchID, String player);

    void leaveMatch(String matchID, String player);

    void startGame(String matchID);

    void addPoints(String matchID, String player, Location location);

   List<Hunt> getHuntsPerUser(String userName);

   Hunt getHuntPerName(String name, String userName);

}
