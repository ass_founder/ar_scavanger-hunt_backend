package de.htwg.moco.arscavangerhunt.repositories.listener;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import de.htwg.moco.arscavangerhunt.constants.DatabaseKeyConstants;
import de.htwg.moco.arscavangerhunt.model.DefaultHuntImpl;
import de.htwg.moco.arscavangerhunt.model.Hunt;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;

import java.util.*;

public class HuntsListenerImpl implements DatabaseListener<Hunt> {

    private List<Hunt> valuesAsList = new ArrayList<>();
    private Map<String, List<Hunt>> valuesAsMap = new HashMap<>();

    @Override
    public String getListenerName() {
        return DatabaseKeyConstants.KEY_HUNTS_PER_NAME;
    }

    @Override
    public void startQuery(FirebaseDatabase database) {

        database.getReference(DatabaseKeyConstants.KEY_HUNT).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Hunt hunt = snapshot.getValue(DefaultHuntImpl.class);
                    valuesAsList.add(hunt);
                    String huntName = snapshot.getKey();
                    List<Hunt> hunts = new ArrayList<>();
                    if(valuesAsMap.get(huntName) != null){
                        hunts = valuesAsMap.get(huntName);
                    }
                    hunts.add(hunt);
                    valuesAsMap.put(huntName, hunts);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public Collection<Hunt> getValues() {
        return valuesAsList;
    }

    @Override
    public Map<?, List<Hunt>> getValuesAsMap() {
        return valuesAsMap;
    }
}
