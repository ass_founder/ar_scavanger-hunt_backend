package de.htwg.moco.arscavangerhunt.repositories.listener;

import com.google.firebase.database.*;
import de.htwg.moco.arscavangerhunt.constants.DatabaseKeyConstants;
import de.htwg.moco.arscavangerhunt.model.Factories.MatchFactory;
import de.htwg.moco.arscavangerhunt.model.multiplayer.Match;
import de.htwg.moco.arscavangerhunt.serivce.LoggerService;
import de.htwg.moco.arscavangerhunt.socket.SocketService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatchNotStartedListenerImpl implements DatabaseListener<Match> {

    private List<Match> values = new ArrayList<>();

    @Override
    public String getListenerName() {
        return DatabaseKeyConstants.KEY_MATCH_NOT_STARTED;
    }

    @Override
    public void startQuery(FirebaseDatabase database) {
        Query startedMatchesQuery = database.getReference(DatabaseKeyConstants.KEY_MATCH).orderByChild("started").equalTo(false);
        startedMatchesQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                values.clear();
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    try{
                        //I have to read it manually because UUID and other classes can´t get parsed
                        Match value = MatchFactory.createMatchFromDataSnapshot(snapshot);
                        values.add(value);
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }
                SocketService.sendMessage("sessions changed");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LoggerService.logWarning(this.getClass().getName(), "Query Canceled");
            }
        });
    }

    @Override
    public List<Match> getValues() {
        return values;
    }

    @Override
    public Map<?, List<Match>> getValuesAsMap() {
        Map<String, List<Match>> valuesAsMap = new HashMap<>();
        valuesAsMap.put(this.getListenerName(), values);
        return valuesAsMap;
    }
}
