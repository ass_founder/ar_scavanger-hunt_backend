package de.htwg.moco.arscavangerhunt.constants;

public final class DatabaseKeyConstants {

    public static final String KEY_LOCATION = "locations";

    public static final String KEY_MATCH = "matchs";

    public static final String KEY_HUNT = "hunts";

    public static final String KEY_USER = "users";

    public static final String KEY_MATCH_NOT_STARTED = "matchNotStarted";

    public static final String KEY_HUNTS_CREATED_USER = "huntsCreatedPerUser";

    public static final String KEY_HUNTS_PER_NAME = "huntsPerName";
}
