#server-actions
Spieler können eine Session erstellen und öffnen
Spieler können eine Liste aller Verfügbaren Sessions öffnen
Zum Testen gibt es derzeit eine Statische Location
Server informiert Spieler über Notifications über Session start
Spieler können gefundene Locations mit Session ID an das Backend senden
Server errechnet ob ein Spieler gewonnen hat
Server informoiert über Notifications die Spieler über den Sieger

Idee: 

Spieler erstellt Session. Server sendet Session ID zurück, über die ID kann mit der Session kommuniziert werden
Spieler öffnet Browser und Wählt Session aus, Session wird mit Session ID angesprochen und Session beigetreten
Notification wird gesendet
Spieler können spielen

#Heroku Workflows
+ Installiern der Heroku CLI
+ ``heroku login``
+ Navigiere in der Konsole zum Backend-Projektordner
+ ``heroku git:remote -a ar-scavenger-hunt``

##Deploy
+ Commit: ``git commit -m "Nachricht"``
+ Push und Deploy: ``git push heroku master``

##Logs
+ Auszug: ``heroku logs``
+ Live: ``heroku logs --tail``